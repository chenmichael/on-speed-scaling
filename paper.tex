\RequirePackage{currfile} 

\documentclass{article}

\usepackage[a4paper]{geometry}

\input{preamble/packages.tex}
\input{preamble/macros.tex}

\title{On speed scaling via integer programming}
\author{Michael Chen and Nicolas von Lützow}
% \institute{Seminar: Advances in Scheduling Algorithms}
\date{09.12.2019}

\newcommand{\bigO}{\mathcal{O}}

\begin{document}

\maketitle
\tableofcontents

\section{Notation}

\begin{tabular}{l p{13cm}}
  $L$ & Number of CPU cycles required by the service load \\
  $n$ & Number of processing units (CPUs or CPU cores) \\
  $\alpha_i$ & Linear energy scale factor for processor $i$ \\
  $\beta_i$ & Exponential energy inefficiency value for processor $i$ \\
  $\gamma_i$ & Activation energy cost for processor $i$ \\
  $x_i$ & $1$ iff processor $i$ is activated, else $0$ \\
  $s_i$ & Number of cycles from the service load calculated by processor $i$
\end{tabular}

\newpage

\section{Introduction}

The paper presents an algorithm for approximating the following class of convex mixed-integer nonlinear programs (convex MINLPs):

\begin{equation*}
    \min_{x,s} \left\{ \sum_{i=1}^n \left( \alpha_is_i^{\beta_i} + \gamma_ix_i \right) \right\}
\end{equation*}
\begin{equation*}
    \sum_{i=1}^n s_i \geq L, Lx_i \geq s_i, s_i \geq 0, x_i \in \left\{ 0,1 \right\} \forall i \in \left\{ 1,\dots,n \right\}
\end{equation*}

\subsection{Problem}

An example for a real life instance of this problem is the data warehouse scheduling problem. It tries to minimize the total energy consumption of a service load in a data warehouse with multiple heterogeneous processing units. Each processing unit can run at some positive speed $s_i$ or it can be disabled for the service load. It consumes $\gamma_i$ energy units to be enabled initially and some variable amount energy depending on the speed ${\alpha_i}{s_i}^{\beta_i}$ where $\alpha_i$ is a linear energy multiplier and $\beta_i$ is an exponential energy inefficiency value.

From now on this introduced class of problems will be referred to by the data warehouse scheduling problem.

\section{Special cases}

\subsection{Similar processors}
\label{ssec:prop1}

If at least two of the processor parameters $\alpha$, $\beta$ or $\gamma$ are equal for all processors the problem is solvable in polynomial time. Note however that if $\beta$ is one of the two fixed parameters the problem can even be solved in linear time (see \ref{ssec:prop2}).

\begin{proof}
If we know how many processors $k = |F|$ are active we can pick the $k$ best processors from the available processors sorted by the variable parameter.

Since the total number of available processors $n$ is constant we can check every $k \in [n]$.
\end{proof}

\subsubsection{Example}

If the processors differ only in $\alpha$ values the most efficient processors are those with the least $\alpha$ value since this value scales the energy consumption linearly.

\subsection{Efficient processors}
\label{ssec:prop2}

If $\beta_i = 1$ for all processors $i \in [n]$ all processors scale linearly in respect to the speed they are running at. It is easy to understand that since speeding up a processor doesn't result in energy loss but only in performance gain.

\begin{proof}
We can just pick the single processor with the best combination of $\alpha$ and $\gamma$ values to calculate the total service load. This said best combination can be calculated as $\min_{i \in n} \paa{\alpha_iL+\gamma_i}$. All processors $i^*$ that satisfy $i^* \in \argmin_{i \in n} \paa{\alpha_iL+\gamma_i}$ will result in a minimal total energy consumtion when the service load is completely calculated on one processing unit.
\end{proof}

\subsubsection{Example}

Consider the following processors and the service load $L=500$:

\begin{table}[H]
    \centering
    \begin{tabular}{R|R|R||R}
        i & \alpha_i & \gamma_i & \text{Energy} \\ \hline
        1 & 3.9 & 394 & 2344 \\
        2 & 1.2 & 57 & 657 \\
        3 & 0.7 & 438 & 788 \\
        4 & 5.0 & 47 & 2547 \\
        5 & 4.2 & 201 & 2301
    \end{tabular}
    \caption{example processor set}
    \label{tbl:prop2example}
\end{table}

Note that even though processor $3$ scales a lot better than processor $2$ its energy consumption is still higher. The optimal solution in this case is to only activate processor $2$ with speed $s_i=L=500$.

\section{NP-Hardness}
\label{sec:prop3}

In essence this problem is a more difficult instance of the subset sum problem which is known to be NP-complete. It's goal is to find a subset of numbers from a set with a sum equal to a specific number. The proposed problem equivalently has to find a subset of processors to activate where the total processor cycles sum up to the service load.

\end{document}